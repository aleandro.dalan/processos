package br.com.aleandro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.aleandro.models.Processo;

public interface ProcessoRepository extends JpaRepository<Processo, Long> {

	List<Processo> findByNrProcessoContaining(String numeroProcesso);
}