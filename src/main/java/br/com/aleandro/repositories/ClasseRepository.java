package br.com.aleandro.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.aleandro.models.Classe;

@Repository
public interface ClasseRepository extends JpaRepository<Classe, Long> {

}