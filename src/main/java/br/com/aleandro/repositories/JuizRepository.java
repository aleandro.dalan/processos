package br.com.aleandro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.aleandro.models.Juiz;

public interface JuizRepository extends JpaRepository<Juiz, Long> {
	List<Juiz> findByNomeIgnoreCaseContaining(String nome);
}