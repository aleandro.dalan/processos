package br.com.aleandro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.aleandro.models.Parte;

public interface ParteRepository extends JpaRepository<Parte, Long> {

	List<Parte> findByNomeIgnoreCaseContaining(String nome);
}