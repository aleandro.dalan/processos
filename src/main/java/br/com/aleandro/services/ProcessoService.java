package br.com.aleandro.services;

import java.util.List;

import br.com.aleandro.models.Processo;

public interface ProcessoService {

	void salvar(Processo processo);
	
	List<Processo> buscarTodosProcessos();
	
	List<Processo> buscarProcessoPorNumero(String numeroProcesso);
}
