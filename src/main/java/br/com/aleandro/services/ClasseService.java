package br.com.aleandro.services;

import java.util.List;

import br.com.aleandro.models.Classe;

public interface ClasseService {
	
	void salvar(Classe classe);
	
	List<Classe> buscarTodasClasse();
}