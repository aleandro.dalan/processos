package br.com.aleandro.services;

import java.util.List;

import br.com.aleandro.models.Parte;

public interface ParteService {

	void salvarParte(Parte parte);
	
	List<Parte> buscarTodasPartes();
	
	List<Parte> buscarPartePorNome(String nome);
}