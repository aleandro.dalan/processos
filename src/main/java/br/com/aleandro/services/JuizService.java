package br.com.aleandro.services;

import java.util.List;

import br.com.aleandro.models.Juiz;

public interface JuizService {
	
	void salvar(Juiz juiz);
	
	List<Juiz> buscarTodosJuizes();
	
	List<Juiz> buscarJuizPorNome(String nome);
}