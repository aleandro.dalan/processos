package br.com.aleandro.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.aleandro.models.Classe;
import br.com.aleandro.repositories.ClasseRepository;
import br.com.aleandro.services.ClasseService;

@Service
public class ClasseServiceImpl implements ClasseService {
	
	@Autowired
	private ClasseRepository repository;
	
	@Override
	public void salvar(Classe classe) {
		repository.save(classe);
	}

	@Override
	public List<Classe> buscarTodasClasse() {
		return repository.findAll();
	}
}