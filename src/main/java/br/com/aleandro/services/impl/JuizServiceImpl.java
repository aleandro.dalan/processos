package br.com.aleandro.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.aleandro.models.Juiz;
import br.com.aleandro.repositories.JuizRepository;
import br.com.aleandro.services.JuizService;

@Service
public class JuizServiceImpl implements JuizService {

	@Autowired
	private JuizRepository juizRepository;
	
	@Override
	public void salvar(Juiz juiz) {
		juizRepository.save(juiz);
	}

	@Override
	public List<Juiz> buscarTodosJuizes() {
		return juizRepository.findAll();
	}

	@Override
	public List<Juiz> buscarJuizPorNome(String nome) {
		return juizRepository.findByNomeIgnoreCaseContaining(nome);
	}
}