package br.com.aleandro.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.aleandro.models.Parte;
import br.com.aleandro.repositories.ParteRepository;
import br.com.aleandro.services.ParteService;

@Service
public class ParteServiceImpl implements ParteService {
	
	@Autowired
	private ParteRepository parteRepository;

	@Override
	public void salvarParte(Parte parte) {
		parteRepository.save(parte);
	}

	@Override
	public List<Parte> buscarTodasPartes() {
		return parteRepository.findAll();
	}

	@Override
	public List<Parte> buscarPartePorNome(String nome) {
		return parteRepository.findByNomeIgnoreCaseContaining(nome);
	}
}