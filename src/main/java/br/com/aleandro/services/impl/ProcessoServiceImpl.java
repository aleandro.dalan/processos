package br.com.aleandro.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.aleandro.models.Processo;
import br.com.aleandro.repositories.ProcessoRepository;
import br.com.aleandro.services.ProcessoService;

@Service
public class ProcessoServiceImpl implements ProcessoService {

	@Autowired
	private ProcessoRepository processoRepository;
	
	@Override
	public void salvar(Processo processo) {
		processoRepository.save(processo);
	}

	@Override
	public List<Processo> buscarTodosProcessos() {
		return processoRepository.findAll();
	}

	@Override
	public List<Processo> buscarProcessoPorNumero(String numeroProcesso) {
		return processoRepository.findByNrProcessoContaining(numeroProcesso);
	}
}