package br.com.aleandro.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class Endereco {

	@NotBlank(message = "Bairro não pode ser vazio;")
	@Column(name = "BAIRRO", length = 100)
	private String bairro;
	
	@JsonProperty("localidade")
	@NotBlank(message = "Cidade não pode ser vazio;")	
	@Column(name = "CIDADE", length = 100)
	private String cidade;
	
	@JsonProperty("uf")
	@NotBlank(message = "Estado não pode ser vazio;")	
	@Column(name = "ESTADO", length = 45)
	private String estado;
	
	@NotBlank(message = "CEP não pode ser vazio;")	
	@Column(name = "CEP", length = 45)
	private String cep;

	@NotBlank(message = "Logradouro não pode ser vazio;")	
	@Column(name = "LOGRADOURO", length = 100)
	private String logradouro;
	
	@Column(name = "NUMERO")
	private Integer numero;
}
