package br.com.aleandro.models;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TB_PARTE")
public class Parte extends AbstractEntity<Long> {

	@NotBlank(message = "Nome não pode estar vazio")
	@Column(name = "NOME", length = 150)
	private String nome;

	@NotNull(message = "Data de nascimento não pode estar vazia")
	@Column(name = "DATA_NASCIMENTO")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataNascimento;
	
	@NotBlank(message = "CPF não pode estar vazio")
	@Column(name = "CPF", length = 11)
	private String cpf;
	
	@NotBlank(message = "Tipo da parte não pode estar vazio")
	@Column(name = "TIPO_PARTE", length = 45)
	private String tipoParte;
	
	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "TB_PROCESSO_PARTE",
			   joinColumns = {@JoinColumn(name = "parte_id")},
			   inverseJoinColumns = {@JoinColumn(name = "processo_id")})
	private List<Processo> processos;

	@Valid	
	@Embedded
	private Endereco endereco;
}
