package br.com.aleandro.models;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.web.JsonPath;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TB_PROCESSO")
public class Processo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nr_processo")
	private String nrProcesso;

	@JsonFormat(pattern = "dd/MM/yyyy")
	@Column(name = "data_criacao")
	private LocalDate dataCriacao;
	
	@JsonFormat(pattern = "dd/MM/yyyy")	
	@Column(name = "data_distribuicao")
	private LocalDate dataDistribuicao;
	
	@NotNull(message = "Classe é obrigatório")
	@ManyToOne
	@JoinColumn(name = "classe_fk")
	private Classe classe;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "processos")
	private List<Juiz> juizes;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "processos")
	private List<Parte> partes;
}