package br.com.aleandro.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TB_CLASSE")
public class Classe {
	
	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "ID_CNJ", length = 20)
	private String idCnj;
	
	@Column(name = "DS_CLASSE", length = 100)
	private String dsClasse;
	
	@Column(name = "SIGLA", length = 45)
	private String sigla;
	
	@Column(name = "TIPO", length = 45)
	private String tipo;

	@JsonIgnore
	@OneToMany(mappedBy = "classe")
	private List<Processo> processos;
}