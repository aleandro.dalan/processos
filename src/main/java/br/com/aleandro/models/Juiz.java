package br.com.aleandro.models;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "TB_JUIZ")
@Entity
public class Juiz extends AbstractEntity<Long> {
	
	@NotBlank(message = "Nome não pode ser vazio")
	@Column(name = "NOME", length = 150)
	private String nome;

	@CPF(message = "CPF inválido")	
	@Column(name = "CPF", length = 11)
	private String cpf;
	
	@NotNull(message = "Data de nascimento não pode ser vazio")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Column(name = "DATA_NASCIMENTO")
	private LocalDate dataNascimento;

	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "TB_PROCESSO_JUIZ",
			   joinColumns = {@JoinColumn(name = "juiz_id")},
			   inverseJoinColumns = {@JoinColumn(name = "processo_id")})
	private List<Processo> processos;
}