package br.com.aleandro;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.aleandro.utils.LerArquivo;

@SpringBootApplication
public class SpringBootInicialization {
	
	@Autowired
	private LerArquivo lerArquivo;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootInicialization.class, args);
	}
	
	@PostConstruct
	public void lerArquivo() {
		lerArquivo.arquivoCsv("TABELA_DE_CLASSES_ATIVAS.csv");
	}
}