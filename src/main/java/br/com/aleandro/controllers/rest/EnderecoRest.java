package br.com.aleandro.controllers.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.aleandro.models.Endereco;

@RestController
@RequestMapping("/enderecos")
public class EnderecoRest {
	private RestTemplate rest = new RestTemplate();	

	@GetMapping("{cep}")
	public Endereco buscarEnderecoPorCep(@PathVariable String cep) {
		cep = cep.replace(".", "").replace("-", "");
		String uri = "https://viacep.com.br/ws/" + cep + "/json/";
		Endereco endereco = rest.getForObject(uri, Endereco.class);

		return endereco;
	}	
}
