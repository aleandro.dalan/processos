package br.com.aleandro.controllers;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.aleandro.models.Processo;
import br.com.aleandro.services.ProcessoService;

@Transactional
@RestController
@RequestMapping("/processos")
public class ProcessoController {
	String mes = String.valueOf(LocalDate.now().plusMonths(1));
	String ano = String.valueOf(LocalDate.now().getYear());
	
	@Autowired
	private ProcessoService processoService;

	@Transactional(readOnly = true)
	@GetMapping
	public List<Processo> buscarTodosProcessos() {
		return processoService.buscarTodosProcessos();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Processo salvarProcesso(@RequestBody @Valid Processo processo) {
		processo.setNrProcesso(String.format ("%07d", processo.getId()) + "-" + mes + "." + ano + "." + "811");
		processoService.salvar(processo);
		
		return processo;
	}
	
	@Transactional(readOnly = true)
	@GetMapping("{numero}")
	public List<Processo> buscarProcessoPorNumero(@PathVariable String numero) {
		return processoService.buscarProcessoPorNumero(numero);
	}
}