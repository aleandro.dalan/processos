package br.com.aleandro.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.aleandro.models.Parte;
import br.com.aleandro.services.ParteService;

@Transactional
@RestController
@RequestMapping("/partes")
public class ParteController {

	@Autowired
	private ParteService parteService;
	
	@Transactional(readOnly = true)
	@GetMapping
	public List<Parte> buscarTodasPartes() {
		return parteService.buscarTodasPartes();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Parte salvarParte(@RequestBody @Valid Parte parte) {
		parteService.salvarParte(parte);
		
		return parte;
	}
	
	@GetMapping("{nome}")
	public List<Parte> buscarPartePorNome(@PathVariable("nome") String nome) {
		return parteService.buscarPartePorNome(nome);
	}
}
