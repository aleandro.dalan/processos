package br.com.aleandro.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.aleandro.models.Classe;
import br.com.aleandro.services.ClasseService;

@Transactional
@RestController
@RequestMapping("/classes")
public class ClasseController {
	
	@Autowired
	private ClasseService classeService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Classe salvarClasse(@RequestBody @Valid Classe classe) {
		classeService.salvar(classe);
		
		return classe;
	}
	
	@Transactional(readOnly = true)
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<Classe> buscarTodasClasse() {
		return classeService.buscarTodasClasse();
	}
}