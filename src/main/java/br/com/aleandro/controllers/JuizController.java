package br.com.aleandro.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.aleandro.models.Juiz;
import br.com.aleandro.services.JuizService;

@Transactional
@RestController
@RequestMapping("/juizes")
public class JuizController {
	
	@Autowired
	private JuizService juizService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Juiz salvarJuiz(@RequestBody @Valid Juiz juiz) {
		juizService.salvar(juiz);
		
		return juiz;
	}
	
	@Transactional(readOnly = true)
	@GetMapping
	public List<Juiz> buscarTodosJuizes() {
		return juizService.buscarTodosJuizes();
	}

	@Transactional(readOnly = true)
	@GetMapping("{nome}")
	public List<Juiz> buscarJuizPorNome(@PathVariable("nome") String nome) {
		return juizService.buscarJuizPorNome(nome);
	}
}
