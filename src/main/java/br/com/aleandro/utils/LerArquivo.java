package br.com.aleandro.utils;

import java.io.File;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.aleandro.models.Classe;
import br.com.aleandro.repositories.ClasseRepository;

@Component
public class LerArquivo {

	@Autowired
	private ClasseRepository classeRepository;
	
    private static final String SEPARADOR = ",";
    private static Classe classe;
    
	static String path = "/home/aleandro/projetos/Processos_TJ/processos/arquivos/";
	
	public void arquivoCsv(String nomeArquivo) {
		try {
			File arquivo = new File(path + nomeArquivo);
			Scanner scanner = new Scanner(arquivo);
			scanner.useDelimiter("\n");
			
			while (scanner.hasNext()) {
				String linha = scanner.next();	
				String[] dadosClasse = linha.split(SEPARADOR);
				
				if (!dadosClasse[0].equalsIgnoreCase("id")) {
					classe = new Classe();
					classe.setId(Long.parseLong(dadosClasse[0]));
					classe.setIdCnj(dadosClasse[1]);
					classe.setDsClasse(dadosClasse[2]);
					classe.setSigla(dadosClasse[3]);
					classe.setTipo(dadosClasse[4]);
					
					classeRepository.save(classe);
				}
			}
			
			scanner.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}